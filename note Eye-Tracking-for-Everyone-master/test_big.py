import os
from load_data import load_data_names, load_batch_from_names#, load_batch_from_names_fixed
from models import get_eye_tracker_model
import numpy as np
import matplotlib.pyplot as plt
import math

def generator(data, batch_size, img_ch, img_cols, img_rows):

    while True:
        for it in list(range(0, data[0].shape[0], batch_size)):
            x, y = load_batch([l[it:it + batch_size] for l in data], img_ch, img_cols, img_rows)
            yield x, y

def dotproduct(v1, v2):
  return sum((a*b) for a, b in zip(v1, v2))

def length(v):
  return math.sqrt(dotproduct(v, v))

def angle(v1, v2):
  return math.acos(dotproduct(v1, v2) / (length(v1) * length(v2)))
def draw_result(prediction, label_y_i, title, vector_angle_degree):
    # plt.plot(lst_iter, lst_loss, '-b', label='loss')
    # plt.plot(lst_iter, lst_acc, '-r', label='accuracy')

    # plt.xlabel("x")
    # plt.ylabel("y")
    # plt.legend(loc='upper left')
    # plt.title(title)
    # plt.savefig(title+".png")  # should before plt.show method


    plt.clf() 
    plt.plot([0,prediction[0]], [0,prediction[1]], color='b', label='prediction')
    plt.plot([0,label_y_i[0]], [0,label_y_i[1]], color='g', label='label')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend(loc='upper left')
    plt.ylim(-20, 25)
    plt.xlim(-20, 20)
    plt.text(-10,21,'Deviation angle' + str(vector_angle_degree) + ' (degree)')
    plt.title(title)
    plt.savefig(title+".png") 
    # plt.show()

def test_big(args):
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = args.dev

    cwd_folder = os.getcwd()
    names_path = cwd_folder + "/dataset/00097/frames/"
    print("Names to test: {}".format(names_path))

    dataset_path = cwd_folder + "/dataset/00099/frames/"
    print("Dataset: {}".format(names_path))

    weights_path = cwd_folder + "/weights_big/weights.005-14.03400.hdf5"
    print("Weights: {}".format(weights_path))

    # image parameter
    img_cols = 64
    img_rows = 64
    img_ch = 3

    # test parameter
    #batch_size = 64
    batch_size = args.batch_size
    chunk_size = 500

    # model
    model = get_eye_tracker_model(img_ch, img_cols, img_rows)

    # model summary
    model.summary()

    # weights
    print("Loading weights...")
    model.load_weights(weights_path)

    # data
    test_names = load_data_names(names_path)

    # limit amount of testing data
    # test_names = test_names[:1000]

    # results
    err_x = []
    err_y = []

    print("Loading testing data...")
    for it in list(range(0, len(test_names), chunk_size)):

        #x, y = load_batch_from_names_fixed(test_names[it:it + chunk_size],  dataset_path, img_ch, img_cols, img_rows)
        dataset_path_folder = dataset_path[:-7]
        x, y = load_batch_from_names(test_names[it:it + chunk_size], dataset_path_folder, img_ch, img_cols, img_rows)
        predictions = model.predict(x=x, batch_size=batch_size, verbose=1)


        prediction_x = []
        prediction_y = []
        label_x = []
        label_y = []

        f=open("save_test_big_file.txt", "a+")

        # print and analyze predictions
        for i, prediction in enumerate(predictions):
            #print("PR: {} {}".format(prediction[0], prediction[1]))
            #print("GT: {} {} \n".format(y[i][0], y[i][1]))

            f.write("PR: {} {}".format(prediction[0], prediction[1]) + "\n")           
            f.write("GT: {} {} \n".format(y[i][0], y[i][1]))

            prediction_x.append(prediction[0])
            prediction_y.append(prediction[1])
            label_x.append(y[i][0])
            label_y.append(y[i][1])
            
            if(y[i][0] == 0 and y[i][1] == 0):
                vector_angle = angle(prediction,[y[i][0], 1])
                vector_angle_degree = vector_angle * 180 / math.pi
                #print("vector_angle of ",i, " : ",vector_angle_degree)
                f.write("angle: {} {} \n".format(vector_angle, vector_angle_degree)+ "\n")
                #draw_result(prediction, y[])
            elif (prediction[0] == 0 and prediction[1] == 0):
                vector_angle = angle([prediction[0],1],y[i])
                vector_angle_degree = vector_angle * 180 / math.pi
                #print("vector_angle of ",i, " : ",vector_angle_degree)
                f.write("angle: {} {} \n".format(vector_angle, vector_angle_degree)+ "\n")
            else:
                vector_angle = angle(prediction,y[i])
                vector_angle_degree = vector_angle * 180 / math.pi
                #print("vector_angle of ",i, " : ",vector_angle_degree)
                f.write("angle: {} {} \n".format(vector_angle, vector_angle_degree)+ "\n")
            image_names_path = cwd_folder + "/chart_image_test/" + str(i)
            draw_result(prediction, y[i],image_names_path, vector_angle_degree)
            err_x.append(abs(prediction[0] - y[i][0]))
            err_y.append(abs(prediction[1] - y[i][1]))

            #print chart


    # mean absolute error
    mae_x = np.mean(err_x)
    mae_y = np.mean(err_y)

    # standard deviation
    std_x = np.std(err_x)
    std_y = np.std(err_y)

    # final results
    print("MAE: {} {} ( samples)".format(mae_x, mae_y))
    print("STD: {} {} ( samples)".format(std_x, std_y))

    f.write("MAE: {} {} ( samples)".format(mae_x, mae_y) + "\n")
    f.write("STD: {} {} ( samples)".format(std_x, std_y))
    f.close()

    prediction_x.append(mae_x)
    prediction_y.append(mae_y)
    label_x.append(std_x)
    label_y.append(std_y)

    # plt.plot(prediction_x, prediction_y, color='g')
    # plt.plot(label_x, label_y, color='orange')
    # plt.xlabel('x')
    # plt.ylabel('y')
    # plt.title("")
    # plt.show()


if __name__ == '__main__':
    test_big()


#thu 4
#ddo lai loi, visualine ket qua, coi lai bai bao do loi nhuw nao, reporst oi nhu nao
#tinh loi goc,  show hinh goc loi
#tensoflow point/ boild